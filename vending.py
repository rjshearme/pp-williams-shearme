class vending_machine(object):
    def __init__(self):
        __self.current_amount = 0
        __self.products = {}
        __self.accepted_coins = set('Nickles', 'Dimes', 'Quarters')

    current_amount = 0
    def insert_coins(self, coins: dict):
        for coin in coins:
            if coin in __self.accepted_coins:
                __self.current_amount += coins[coin]
            else:
                print("You can't enter {}s!".format(coin))

    def add_products(self):
        pass


    def display_products(self):
        pass


    def buy_product(self):
        pass


    def return_coins(self):
        pass


def test_vending_machine():
    vending_machine.insert
